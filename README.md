To run the presentation, you will need `presenterm`.
You can get it [here](https://github.com/mfontanini/presenterm).

Run the presentation with
```shell
$ presenterm crustacean_band.md
```

If you just want to learn Rust, check out these links:

- [The Rust Book](https://doc.rust-lang.org/stable/book/title-page.html)
- [Half Hour to Learn Rust](https://fasterthanli.me/articles/a-half-hour-to-learn-rust)
- [Learn Rust with Entirely Too Many Linked Lists](https://rust-unofficial.github.io/too-many-lists/)
- [Rustlings](https://github.com/rust-lang/rustlings)
