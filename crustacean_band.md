---
title: Intro to Rust, the Hot Crustacean Band
author: George Hosono
date: 2023-11-03
override:
  default:
    table:
      alignment: center
---

The plan for today
---
- Explain what Rust is
- Discuss the importance of memory safety
- See how Rust ensures memory safety with no runtime performance penalty
- Look at some features that make developing in Rust really nice

<!-- end_slide -->

tldr: Don't start new projects in unsafe languages
---
- Memory safe languages (e.g. Go and Rust) are easy and fast
- Memory safety bugs are terrible
- Life's too short for segfaults

<!-- end_slide -->

What is Rust?
---
<!-- pause -->
- Systems Programming Language
<!-- pause -->
- Memory Safe
<!-- pause -->
- No Runtime or Garbage Collector
<!-- pause -->
- Blazingly Fast 🚀
<!-- pause -->
- Eliminates entire classes of bugs
<!-- pause -->
- The most helpful compiler you've met
<!-- pause -->
- Does nothing new
<!-- end_slide -->

Does nothing new?
---
Almost all the ideas in Rust come from another programming language
| **Rust Feature**                      | **Origin**              |
| --                                    | --                      |
| Resourse Acquisition is Initializtion | C++                     |
| Lifetimes                             | Cyclone                 |
| Algebraic Data Types                  | OCaml, SML              |
| Traits                                | Haskell                 |
| Functional Programming                | Lisp                    |
| Channels                              | Newsqueak, Alef, Limbo  |
| Optional Bindings                     | Swift                   |
| Hygenic Macros                        | Scheme                  |
| and more!                             |                         |
<!-- end_slide -->

What is Memory Safety?
---
The property of having all pointers/references point to valid objects of the correct type.

<!-- pause -->
# Examples of Memory Safety Violations
## Pointer to the wrong type
```c
int x = 0;
long *p = (long *)&x;
```

<!-- pause -->
## Pointer to invalid memory
```c
char buf[10];
char *p = &buf[10];
```
<!-- end_slide -->

Other Memory Safety Violations
---
<!-- column_layout: [1, 1] -->

<!-- column: 0 -->
Use after free
```c
int *p = (int *)malloc(sizeof(int));
free(p);
*p = 42; // BAD!
```

Double free
```c
int *p = (int *)malloc(sizeof(int));
free(p);
free(p)
```

<!-- column: 1 -->
Data Race
```c++
void do_stuff(int *p) {
  *p = 3;
}

int x;
std::thread t(do_stuff, &x);
x = 4;
t.join();
```

<!-- end_slide -->

The Importance of Memory Safety
---
Around 70% of vulnerabilities in Microsoft products are memory safety issues.

And around 70% of serious bugs in Chromium are too!

These are easy mistakes to make, and they have a huge impact.

[](https://msrc.microsoft.com/blog/2019/07/a-proactive-approach-to-more-secure-code/)
[](https://www.chromium.org/Home/chromium-security/memory-safety/)
<!-- end_slide -->

The Difficulty of Memory Safety
---
Spot the bug
```c
int main() {
  int buf[100];
  for (int i = 0; i <= 100; i++) {
    buf[i] = i;
  }
  return 0;
}  
```

<!-- end_slide -->

How to Acheive Memory Safety
---
<!-- pause -->
- Trust the Programmer
  - aka Manual Memory Management
<!-- pause -->
- Tracing Garbage collection
  - Easy for the programmer, but has intermittent performance penalties
<!-- pause -->
- Automatic Reference Counting
  - Usually more performant than a Tracing GC
  - Requires the programmer to avoid reference cycles
<!-- pause -->
- Borrow Checking
  - No performance penalty!
  - Can be more work for the programmer
<!-- end_slide -->

Languages by Memory Management System
---
<!-- column_layout: [1, 2, 1] -->
<!-- column: 1 -->
| Style                        | Languages   |
| -----                        | -----       |
| Manual Memory Management     | C           |
|                              | C++         |
|                              | Zig (kinda) |
| -----                        | -----       |
| Tracing Garbage Collector    | Go          |
|                              | Java        |
|                              | C#          |
|                              | JavaScript  |
| -----                        | -----       |
| Automatic Reference Counting | Python      |
|                              | Perl        |
|                              | Swift       |
| -----                        | -----       |
| Borrow Checking              | Rust        |
<!-- end_slide -->

Memory Management in Rust
---
- Every value has an owner
- There is only ever one owner
- When an owner goes out of scope, everything it owns is dropped

<!-- pause -->
<!-- column_layout: [1, 1] -->
<!-- column: 0 -->
## Single owner
```rust
fn do_stuff(wig: Widget) {
  // snip 
}

fn main() {
  let w = Widget::new();
  do_stuff(w);
  println!("{}", w); // Compiler error
}
```

<!-- pause -->
<!-- column: 1 -->
## Owners can share values through references
```rust
fn do_stuff_ref(wig: &Widget) {
  // snip 
}

fn main() {
  let w = Widget::new();
  do_stuff_ref(&w);
  println!("{}", w); // This is fine now
}
```
<!-- end_slide -->

References and Borrowing
---
References must always be valid

```rust
fn main() {
  let x = 0;
  drop(x);
  let p = &x; // compiler error
}
```
<!-- end_slide -->

References and Borrowing
---
You can have as many immutable references as you want

```rust
fn main() {
  let x = 0;
  let p1 = &x;
  let p2 = &x;
  let p3 = &x;
}
```
<!-- end_slide -->

References and Borrowing
---
You may only have one mutable reference at a time

```rust
fn main() {
  let x = 0;
  let p1 = &mut x;
  let p2 = &x; // compiler error
}
```
<!-- end_slide -->

So What is the Borrow Checker?
---
# Example from the Rust Book
You can't have a reference to something after the thing has been dropped.
<!-- column_layout: [1, 1] -->
<!-- column: 0 -->
```rust
// Does not compile
fn main() {
    let r;                // ---------+-- 'a
                          //          |
    {                     //          |
        let x = 5;        // -+-- 'b  |
        r = &x;           //  |       |
    }                     // -+       |
                          //          |
    println!("r: {}", r); //          |
}                         // ---------+
```

<!-- column: 1 -->
```rust
// Does compile
fn main() {
    let x = 5;            // ----------+-- 'b
                          //           |
    let r = &x;           // --+-- 'a  |
                          //   |       |
    println!("r: {}", r); //   |       |
                          // --+       |
}                         // ----------+
```
<!-- end_slide -->

What happens when you try to write unsafe code in Rust?
--- 
<!-- column_layout: [3, 7] -->

<!-- column: 0 -->
## Pointer to the wrong type: C
```c
int x = 0;
long *p = (long *)&x;
```

<!-- column: 1 -->
```rust
let x: i32 = 0;
let p: &i64 = &x;
```
```zsh
$ cargo run
error[E0308]: mismatched types
 --> src/main.rs:3:19
  |
3 |     let p: &i64 = &x;
  |            ----   ^^ expected `&i64`, found `&i32`
  |            |
  |            expected due to this
  |
  = note: expected reference `&i64`
             found reference `&i32`
```
<!-- end_slide -->

What happens when you try to write unsafe code in Rust?
--- 

## Pointer to invalid memory

```rust
let buf: [char; 10] = ['a'; 10];
let p = &buf[10];
```

```bash
$ cargo run
error: this operation will panic at runtime
 --> src/main.rs:3:14
  |
3 |     let p = &buf[10];
  |              ^^^^^^^ index out of bounds: the length is 10 but the index is 10
  |
  = note: `#[deny(unconditional_panic)]` on by default
```
<!-- end_slide -->

What is panic?
---
- Panic represents an unrecoverable runtime error
- Usually crashes the program to prevent something bad from happening
- Generally, you should try to handle errors rather than panicking
<!-- end_slide -->

What happens when you try to write unsafe code in Rust?
--- 
## Use after free

```rust
let wig = Box::new(Widget::new());
drop(wig);
do_stuff(*wig);
```

```bash
$ cargo run
error[E0382]: use of moved value: `*wig`
 --> src/main.rs:4:14
  |
2 |     let wig = Box::new(Widget::new());
  |         --- move occurs because `wig` has type `Box<Widget>`, which does not implement the `Copy` trait
3 |     drop(wig);
  |          --- value moved here
4 |     do_stuff(*wig);
  |              ^^^^ value used here after move
```
<!-- end_slide -->

What happens when you try to write unsafe code in Rust?
--- 
## Double Free

```rust
let wig = Box::new(Widget::new());
drop(wig);
drop(wig);
```

```bash
$ cargo run
error[E0382]: use of moved value: `*wig`
 --> src/main.rs:4:10
  |
2 |     let wig = Box::new(Widget::new());
  |         --- move occurs because `wig` has type `Box<Widget>`, which does not implement the `Copy` trait
3 |     drop(wig);
  |          --- value moved here
4 |     drop(*wig);
  |          ^^^^ value used here after move
```
<!-- end_slide -->

What happens when you try to write unsafe code in Rust?
--- 
## Data Race

```rust
let mut wig = Widget::new();
let t = thread::spawn(move || do_stuff(&mut wig));
wig.data = 3;
t.join().unwrap();
```

```bash
$ cargo run
error[E0382]: assign to part of moved value: `wig`
 --> src/main.rs:9:5
  |
7 |     let mut wig = Widget::new();
  |         ------- move occurs because `wig` has type `Widget`, which does not implement the `Copy` trait
8 |     let t = thread::spawn(move || do_stuff(&mut wig));
  |                           -------               --- variable moved due to use in closure
  |                           |
  |                           value moved into closure here
9 |     wig.data = 3;
  |     ^^^^^^^^^^^^ value partially assigned here after move
```
<!-- end_slide -->

Algebraic Data Types
---
Types that let you combine other types

## Product Types: `struct`
```rust
struct Widget {
  id: i32,
  name: String,
  // etc
}
```

## Sum Types
```rust
enum IntOrBool {
  Int(i32),
  Bool(bool),
}
```

<!-- end_slide -->

Option and Result
---
<!-- column_layout: [1, 1] -->
<!-- column: 0 -->
## Option
```rust
enum Option<T> {
  Some(T),
  None,
}
```

<!-- column: 1 -->
## Result
```rust
enum Result<T, E> {
  Ok(T),
  Err(E),
}
```
<!-- end_slide -->

Error Handling in Rust
---
<!-- column_layout: [1, 1] -->
<!-- column: 0 -->
## C Style
```c
int fd = open("filename", O_CLOEXEC | O_RDONLY);
if (fd == -1) {
  if (errno == EACCES) {
    // Handle error
  } else if (errno == ENOENT) {
    // Handle another error
  }
  // ...
}
```
<!-- column: 1 -->
## Rust Style
```rust
fn main() -> std::io::Result<()> {
    let f = match File::open("filename") {
        Ok(f) => f,
        Err(e) => return Err(e),
    };
    Ok(())
}
```
More simply
```rust
fn main() -> std::io::Result<()> {
    let f = File::open("filename")?;
    Ok(())
}
```
<!-- end_slide -->

Unsafe Rust
---
- Sometimes you know better than the borrow checker
- Grants several powers
  - Dereference a raw pointer
  - Call an unsafe function or method
  - Access or modify a mutable static variable
  - Implement an unsafe trait
  - Access fields of unions
  - That's it!
- You still get the Borrow Checker and most safety features
- Required to implement things like data structures

<!-- end_slide -->

Weaknesses of Rust
---
- Anything with large amounts of shared mutable state
  - GUI Programming
  - Data Structures
- Learning Curve
- Rapid prototyping
- Unsafe Rust can be harder than C
<!-- end_slide -->

More to Talk About
--- 
- Traits
- Generics
- Iterators
- Functional Programming
- Concurrency
- Dependency Management
- Standard Library
- `no_std` programming
<!-- end_slide -->

Resources for Learning Rust
---

The Rust book:
[](https://doc.rust-lang.org/stable/book/title-page.html)

Half Hour to Learn Rust:
[](https://fasterthanli.me/articles/a-half-hour-to-learn-rust)

Learn Rust with Entirely Too Many Linked Lists:
[](https://rust-unofficial.github.io/too-many-lists/)

Rustlings Tutorial:
[](https://github.com/rust-lang/rustlings)
<!-- end_slide -->

Thank you!
---